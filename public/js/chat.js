var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'join':
      besede.shift();
      var kanal = besede.join(' ');
      this.spremeniKanal(kanal);
      break;
    case 'username':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'whisper':
      besede.shift();
      var besedilo = besede.join(' ');
      console.log(besedilo);
      var parametri = besedilo.split('\"');
      console.log(parametri);
      if (parametri) {
        this.socket.emit('sporocilo', { vzdevek: parametri[1], besedilo: parametri[3] });
        sporocilo = '(private for ' + parametri[1] + '): ' + parametri[3];
      } else {
        sporocilo = 'Unknown command.';
      }
      break;
    case 'nudge':
      besede.shift();
      var vzdevek = besede.join(' ');
      if (vzdevek) {
        this.socket.emit('dregljaj', { vzdevek: vzdevek });
        sporocilo = 'Nudge ' + vzdevek;
      } else {
        sporocilo = 'Unknown command.';
      }
      break;
    case 'status':
      besede.shift();
      var status = besede.join(' ');
      if (status) {
        this.socket.emit('statusSpremembaZahteva', status);
      } else {
        sporocilo = 'Unknown command.';
      }
      break;
    default:
      sporocilo = 'Unknown command.';
      break;
  };

  return sporocilo;
};

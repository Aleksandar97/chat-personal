function divElementEnostavniTekst(sporocilo) {
  var div = document.createElement("div");
  div.style.cssText = 'font-weight: bold;';
  var wasEmoji = false;
  var words = sporocilo.split(" ");
  for(var i=0;i<words.length;i++){
    for(var j=0;j<emojis.length;j++){
      if(words[i]==emojis[j].text && !wasEmoji){
        $(div).append(emojis[j].code);
        wasEmoji=true;
      }
    }
    if(!wasEmoji){
      $(div).append(document.createTextNode(words[i]+" "));
    }
    wasEmoji=false;
  }
  return div;
  
  
}
function divElementEnostavniTekstKanal(sporocilo) {
 
  return $('<div style="font-weight: bold;"></div>').text(sporocilo);
}

function divElementEnostavniTekstUporabniki(sporocilo) {
  if(sporocilo.stat=="Busy")
    return $('<div style="font-weight: bold;background-color: lightpink;"></div>').text(sporocilo.name);
  if(sporocilo.stat=="Online")
    return $('<div style="font-weight: bold;background-color: lightgreen;"></div>').text(sporocilo.name);
  else
    return $('<div style="font-weight: bold;background-color: lightgray;"></div>').text(sporocilo.name);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

//Find picture
function prepoznajInDodajSlike(sporocilo) {
  var povezave = sporocilo.match(new RegExp("\\bhttps?://\\S+.(jpg|png|gif|jpeg)\\b", "g"));
  if (povezave) {
    for (var i = 0; i < povezave.length; i++) {
      $('#sporocila').append('<div><img src="' + povezave[i] + '" width="200px" style="margin-left: 20px;" /></div>');
    }
  }
}

//Find youtube videos
function prepoznajInDodajYoutubePosnetke(sporocilo) {
  var povezave = sporocilo.match(new RegExp("\\bhttps://www.youtube.com/watch\\?v=\\w+\\b", "g"));
  if (povezave) {
    for (var i = 0; i < povezave.length; i++) {
      $('#sporocila').append('<div><iframe width="200" height="150" style="margin-left: 20px;" src="' + povezave[i].replace("watch?v=", "embed/") + '" frameborder="0" allowfullscreen></iframe></div>');
    }
  }
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst("You: "+sporocilo));
    prepoznajInDodajSlike(sporocilo);
    prepoznajInDodajYoutubePosnetke(sporocilo);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }
  
  $('#poslji-sporocilo').val('');
}

var socket = io.connect();
var trenutniVzdevek = "", trenutniKanal = "";
var emojis;


var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n');
});

//Filter vulgar words
function filtirirajVulgarneBesede(vhod) {
  for (var i in vulgarneBesede) {
    vhod = vhod.replace(new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi'), function() {
      var zamenjava = "";
      for (var j=0; j < vulgarneBesede[i].length; j++)
        zamenjava = zamenjava + "*";
      return zamenjava;
    });
  }
  return vhod;
}

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'You are logged in as ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  //Change color according to status
  socket.on('statusSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      var trenutenStatus = rezultat.status;
      if (trenutenStatus == 'Online') {
        $('#kanal').css('background-color', 'lightgreen');
      } else if (trenutenStatus == 'Away') {
        $('#kanal').css('background-color', 'lightgray');
      } else {
        $('#kanal').css('background-color', 'lightpink');
      }
      sporocilo = 'Your new status is ' + trenutenStatus + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Channel changed.'));
  });
  
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
    prepoznajInDodajSlike(sporocilo.besedilo);
    prepoznajInDodajYoutubePosnetke(sporocilo.besedilo);
  });
  
  socket.on('dregljaj', function (dregljaj) {
    $('#vsebina').jrumble();
    $("#vsebina").trigger('startRumble');
    setTimeout(function(){$("#vsebina").trigger('stopRumble');}, 1500)
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekstKanal(kanal));
      }
    }
    
    //Insta join channel
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/join ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  socket.on('uporabniki', function(uporabniki,userStatuses) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      for(var j=1; j < userStatuses.length; j++) {
        if(userStatuses[j].name==uporabniki[i]){
          $('#seznam-uporabnikov').append(divElementEnostavniTekstUporabniki(userStatuses[j]));
        }
      }
    }
    
    //Insta whisper command 
    $('#seznam-uporabnikov div').click(function() {
      $('#poslji-sporocilo').val('/whisper "' + $(this).text() + '" ');
      $('#poslji-sporocilo').focus();
  });
  });

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    if(fromTimeOut){
      klepetApp.procesirajUkaz('/status Online');
    }
    clearTimeout(autoAway);
    autoAway = setTimeout(function(){ 
      klepetApp.procesirajUkaz('/status Away');
      fromTimeOut = true;
    }, 300000);
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
  
  $.getJSON("/Emojis.json", function(odgovor) {
		emojis = odgovor;
	});
	
	var fromTimeOut = false;
	var autoAway = setTimeout(function(){ 
    klepetApp.procesirajUkaz('/status Away');
    fromTimeOut = true;
  }, 300000);
  
});

//Insert emoji
$('#emoticons span').click(function() {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo=sporocilo+$(this).text();
  $('#poslji-sporocilo').val(sporocilo);
  $('#poslji-sporocilo').focus();
});

//Inser value of command
$('#commands input').click(function() {
  $('#poslji-sporocilo').val("/"+$(this).val().toLowerCase()+" ");
  $('#poslji-sporocilo').focus();
});


var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var userStatuses = [];

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Pub');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajDregljaj(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZahtevoZaSprememboStatusa(socket, vzdevkiGledeNaSocket);
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('uporabniki', function(kanal) {
      var uporabnikiNaKanalu = io.sockets.clients(kanal.kanal);
      var uporabniki = [];
      for (var i=0; i < uporabnikiNaKanalu.length; i++) {
        uporabniki[i] = vzdevkiGledeNaSocket[uporabnikiNaKanalu[i].id];
      }
      socket.to(kanal.kanal).emit('uporabniki', uporabniki, userStatuses);
    });
  });
};

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Guest' + stGosta;
  vzdevki[socket.id] = vzdevek;
  userStatuses[stGosta]={name: vzdevek ,stat:'Online'};
  
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  socket.emit('pridruzitevOdgovor', {kanal: kanal});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' joined the channel ' + kanal + '.'
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Current users on channel ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';
        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Guest') == 0 || vzdevek.indexOf('You') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: "You can't name yourself Guest."
      });
    }if (vzdevek.length <2) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: "Your username has to have more than 1 latter."
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        for(var i =1;i<userStatuses.length;i++){
          if(userStatuses[i].name ==prejsnjiVzdevek){
            userStatuses[i].name =vzdevek;
          }
        }
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' changed his name to ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Username exists.'
        });
      }
    }
  });
}

function obdelajZahtevoZaSprememboStatusa(socket, vzdevkiGledeNaSocket) {
  socket.on('statusSpremembaZahteva', function(status) {
   
      if (status == 'Online' || status == 'Away' || status == 'Busy') {
        var vzdevek = vzdevkiGledeNaSocket[socket.id];
        
        for(var i=1;i<userStatuses.length;i++){
          if(userStatuses[i].name == vzdevkiGledeNaSocket[socket.id]){
            userStatuses[i].stat=status;
          }
        }
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: vzdevek + ' changed his status to ' + status + '.'
        });
        
        socket.emit('statusSpremembaOdgovor', {
          uspesno: true,
          status: status
        });
      } else {
        socket.emit('statusSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Status is not defined.'
        });
      }
    
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    if (sporocilo.kanal) {
      socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
        besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
      });
    } else if (sporocilo.vzdevek) {
      var socketIdNaslovnika = null;
      for (var id in vzdevkiGledeNaSocket) {
       if (sporocilo.vzdevek == vzdevkiGledeNaSocket[id]) {
         socketIdNaslovnika = id;
         break;
       }
      }
      if (socketIdNaslovnika) {
       if (socketIdNaslovnika == socket.id) {
         io.sockets.sockets[socket.id].emit('sporocilo', {
           besedilo: "Massage '" + sporocilo.besedilo + "' to the client with username '" + sporocilo.vzdevek + "' was not able to receve the massage."
         });
       } else {
         io.sockets.sockets[socketIdNaslovnika].emit('sporocilo', {
           besedilo: vzdevkiGledeNaSocket[socket.id] + ' (private): ' + sporocilo.besedilo
         });
       }
      } else {
       io.sockets.sockets[socket.id].emit('sporocilo', {
         besedilo: "Massage '" + sporocilo.besedilo + "' to the client with username '" + sporocilo.vzdevek + "' was not able to receve the massage."
       });
      }
    }
  });
}

function obdelajDregljaj(socket) {
  socket.on('dregljaj', function (dregljaj) {
    if (dregljaj.vzdevek) {
      var socketIdNaslovnika = null;
      for (var id in vzdevkiGledeNaSocket) {
       if (dregljaj.vzdevek == vzdevkiGledeNaSocket[id]) {
         socketIdNaslovnika = id;
         break;
       }
      }
      if (socketIdNaslovnika) {
       if (socketIdNaslovnika == socket.id) {
         io.sockets.sockets[socket.id].emit('sporocilo', {
           besedilo: "The client with username '" + dregljaj.vzdevek + "' was not able to nudge."
         });
       } else {
         io.sockets.sockets[socketIdNaslovnika].emit('dregljaj', { dregljaj: true });
       }
      } else {
       io.sockets.sockets[socket.id].emit('sporocilo', {
         besedilo: "The client with username '" + dregljaj.vzdevek + "' was not able to nudge."
       });
      }
    }
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    socket.leave(trenutniKanal[socket.id]);
    pridruzitevKanalu(socket, kanal.novKanal);
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}
